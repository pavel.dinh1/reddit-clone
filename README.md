# Reddit Clone

Project followed by website : https://programmingtechie.com/2020/05/14/building-a-reddit-clone-with-spring-boot-and-angular/

## Using:  
    * java 11
    * spring boot 2.3.4
    * IntelliJ 2020 Community
    * Angular
    * MySQL

## Tested:
    * Postman
    * Mailtrap.io